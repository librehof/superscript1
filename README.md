## Superlib 1

Apache 2.0 (C) 2023 [librehof.org](http://librehof.org)

- For run-anywhere setup scripts (software bootstrapping and more)
- Layer on top of POSIX shell (sh, dash, bash) in a Unix-like environment
- Error handling, Logging, Basic string functions and more
- [Legal notice](NOTICE) 


### Compatibility

- Works on standard distros like Debian, Ubuntu, Arch, ...
- Works with minimalistic BusyBox systems (like Alpine)
- Enables multi-distro package installation
  - use [command-not-found.com](https://command-not-found.com) to find multi-distro package names
- Can be used with [Git for Windows](https://gitforwindows.org)
  - without the multi-distro package feature (for now)
  - symlinks may require elevation and registry tweaking


### Details

- **[superlib](doc/superlib.md)**
- **[coding](doc/coding.md)**
- **[template](doc/template.sh)**
- **[changelog](doc/changelog.md)**


### Get

```
# download
git clone --depth 2 https://gitlab.com/librehof/superlib1.git
cd superlib1

# update downloaded
cd superlib1
git pull --depth 2

# remove downloaded
rm -rf superlib1
```


### Use in-place

- **copy** bin/superlib to the location of your script
- **import** superlib inside your script (see [template](doc/template.sh))
    

### Install system-wide

```
# update downloaded
cd superlib1
git pull --depth 2

# install
cd superlib1
sudo ./install

# uninstall
sudo rm-superlib
```
